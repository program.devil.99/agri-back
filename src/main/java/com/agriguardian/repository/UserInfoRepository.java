package com.agriguardian.repository;

import com.agriguardian.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    void deleteByAppUserId(Long appUserId);
    UserInfo getUserInfoByAppUserId(Long appUserId);

}
