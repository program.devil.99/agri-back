package com.agriguardian.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteDevicesDto {

    @NotNull(message = "field 'macAddresses' may not be null")
    private Set<String> macAddresses;
}
