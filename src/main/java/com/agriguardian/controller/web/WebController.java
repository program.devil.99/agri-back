package com.agriguardian.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WebController {

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public static String redirectExample(HttpServletRequest request) {
        return "redirect:" + request.getScheme() +"://localhost:3000";
    }
}
