package com.agriguardian.exception;

public class UserFromTokenDoesNotExistsException extends RuntimeException {
    public UserFromTokenDoesNotExistsException(String msg) {
        super(msg);
    }

}
