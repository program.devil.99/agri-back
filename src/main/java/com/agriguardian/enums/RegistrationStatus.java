package com.agriguardian.enums;

public enum RegistrationStatus {
    REGISTRATION,
    ACTIVATED,
    TRIAL,
    SUBSCRIPTION,
    NEED_PAYMENT,
    DEACTIVATED;

}