package com.agriguardian.enums;

public enum SubscribeStatus {
    HAVE_SUB,
    HAVE_NOT_SUB
}
